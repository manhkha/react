import Slide, { Add } from '../slide';
describe('Slide.js', () => {
    const slide = new Slide(
        'n1',
        'n2',
        'n3',
        'n4',
        0
    );

    it('return title', () => {
        expect(slide.title).toBe('n1');
    });
    it('return caption', () => {
        expect(slide.caption).toBe('n2');
    });
    it('return img_link', () => {
        expect(slide.img_link).toBe('n3');
    });
    it('return title', () => {
        expect(slide.link).toBe('n4');
    });
    it('return title', () => {
        expect(slide.id).toBe('slide' + 0);
    });
})
describe('Add()', () => {
    it('return a Number', () => {
        expect(Add(3,2)).toBe(5);
    })
    
    it('return a String', () => {
        expect(Add('abc', 5)).toBe('abc5');
    })
})