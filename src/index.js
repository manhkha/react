import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter } from 'react-router-dom'
import { store } from './store/index';

const render = () => {
    ReactDOM.render(
        <BrowserRouter>
            <App />
        </BrowserRouter>
    , document.getElementById('root'));
}
render();
store.subscribe(render);
registerServiceWorker();
