import React from 'react';
import Home from '../components/home';
import ShowCategory from '../components/categories/ShowCategory';
import { Switch, Route  } from 'react-router-dom';
import Posts from '../components/posts';

const Router = () => (
               <Switch>
                   <Route exact path='/' component={Home} />
                   <Route path='/categories/:id' component={ShowCategory} />
                   <Route path='/posts/:id'component={Posts} />
               </Switch>
        );


export default Router;