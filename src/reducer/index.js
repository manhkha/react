const initState = { 
  posts: [],
  infoUser: {
    name: '',
    email: '',
    password: '',
    password_confirmation: ''
  },
  errors: '',
  post: {},
  category: {
    name: '',
    posts: []
  },
  categories: [],
  currentPost: {}
};

export const reducer = (state = initState, action) => {
  switch (action.type) {
    case 'SIGN_UP':
      return {
        ...state,
        infoUser: {
          name: action['name'],
          email: action['email'],
          password: action['password'],
          password_confirmation: action['password_confirmation']
        }
      };
    case 'ERROR':
      return {
        ...state,
        errors: action.errorMessage
      };
    case 'ALL_POST':
      return {
        ...state,
        posts: action.payload,
        currentPost: action.payload[0]
      };
    case 'DATA_POST':
      return {
        ...state,
        post: action.payload
      };
    case 'DATA_CATEGORY':
      return {
        ...state,
        category: {
          name: action.payload.name,
          posts: action.payload.posts
        }
      };
    case 'PRE_CURRENTPOST':
    case 'NEXT_CURRENTPOST':
      return {
        ...state,
        currentPost: action.payload
      };
    case 'DATA_CATEGORIES':
      return {
        ...state,
        categories: action.payload
      }
    default: 
      return state;
  }
}
