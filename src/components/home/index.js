import React, { Component } from 'react';

import Slider from '../shared/layouts/Slider';
import Post from '../shared/layouts/Post';
import BlogHome from '../shared/layouts/BlogHome';
import CreatedPost from '../shared/layouts/CreatedPost'

class Home extends Component {
    render() {
        return (
            <div>
                <Slider />
                <Post />
                <main role="main" className="container">
                    <div className="row">
                        <BlogHome />
                        <CreatedPost />
                    </div>
                </main>
            </div>
        );
    }
}

export default Home;