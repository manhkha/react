import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { store } from '../../store/index';

class ShowCategory extends Component {
    
    componentDidMount() {
        this.getPostData(this.props.match.params.id);
    }

    getPostData = (category_id) => {
        axios.get(`https://blogapireact.herokuapp.com/categories/${category_id}`)
                .then(
                    res => {
                        store.dispatch(this.getDataCategory(res.data))
                    }
                )
                .catch(function (error) {
                    console.log(error);
                });
    }

    getDataCategory = (data) => {
        return {
            type: 'DATA_CATEGORY',
            category_id: this.props.match.params.id,
            payload: data
        }
    }

    render() {
        let { posts } = store.getState().category;
        let htmlData = '';
        if (posts.length > 0) {
            htmlData = (
                <div>
                    <ol className="list-group">
                        {posts.map((post,index) => <li key={post.id} className="list-group-item list-group-item-success"><Link to={'/posts/' + posts[index].id}>{post.title}</Link></li>)}
                    </ol>
                </div>
            );
        } else {
            htmlData = 'No posts in category';
        }
        return (
            <div>
                <h1>This is {store.getState().category.name}</h1>
                <h1>List posts: {store.getState().category.posts.length}</h1>
                {htmlData}
            </div>
        );
    }
}

export default ShowCategory;
