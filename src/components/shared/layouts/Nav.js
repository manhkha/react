import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { store } from '../../../store/index'

class Nav extends Component {
    componentDidMount(){
        axios.get('https://blogapireact.herokuapp.com/categories')
        .then(
            res => {
                const categories = res.data.categories;
                store.dispatch(this.getDataCategories(categories));
            }
        )
        .catch(function(error) {
            console.log(error);
        });
    }

    getDataCategories = (data) => {
        return {
            type: 'DATA_CATEGORIES',
            payload: data
        }
    }
    
    render() {
        let htmlData =  <div className='loading'>
                            <img src="https://icons8.com/preloaders/generator.php?filmstrip&amp;image=30&amp;speed=9&amp;fore_color=000000&amp;back_color=FFFFFF&amp;size=160x20&amp;transparency=0&amp;reverse=0&amp;orig_colors=0&amp;gray_transp=1&amp;image_type=2&amp;inverse=0&amp;flip=0&amp;frames_amount=13&amp;word=237-261-157-41-266-237-41-257-237-266-57-41-227-41-36-36-36&amp;uncacher=42.95799382614014" alt="load"/>
                        </div>;
      const {categories} = store.getState();
      if (categories.length > 0){
          htmlData = (<div className="nav-scoller py-1 mb-2">
            <nav className="nav d-flex">
            { categories.map(category => <Link key={category.id} className="p-2 " to={'/categories/' + category.id}>{category.name}</Link>)}
            </nav>
          </div>)
      }
        return (
            htmlData
        );
    }
}

export default Nav;