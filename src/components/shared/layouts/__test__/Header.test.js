import React from 'react';
import Header from '../Header';
import renderer from 'react-test-renderer';

describe('components/shared/layout/Header', () => {
    it('match', () => {
        const component = renderer.create(
            <Header />
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});