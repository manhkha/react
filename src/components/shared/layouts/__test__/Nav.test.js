import React from 'react';
import Nav from '../Nav';
import renderer from 'react-test-renderer';

describe('components/shared/layouts/Nav', () => {
    it('match', () => {
        const component = renderer.create(
            <Nav />
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});