import React, { Component } from 'react';
import axios from 'axios';
import { store } from '../../../store/index';

class BlogHome extends Component {
    componentDidMount(){
      axios.get('https://blogapireact.herokuapp.com/allpost')
            .then(res => {
              store.dispatch(this.getDataPosts(res.data.posts))
            })
            .catch(error => {
              console.log(error);
            })
    }

    getDataPosts = (data) => {
        return {
            type: 'ALL_POST',
            payload: data
        }
    }

    prePost = () => {
        let { currentPost, posts } = store.getState();
        for(var i = 0; i < posts.length - 1 ; i++ ){
            if ( currentPost.id === posts[i].id ){
                store.dispatch({ type: 'PRE_CURRENTPOST', payload: posts[i+1]});
            }
        }
    }

    nextPost = () => {
        let { currentPost, posts } = store.getState();
        for(var i = 0; i < posts.length ; i++ ){
            if ( currentPost.id === posts[i].id && i !== 0 ){
                store.dispatch({ type: 'NEXT_CURRENTPOST', payload: posts[i-1]});
            }
        }
    }
  render() {
      let { currentPost } = store.getState();
    return (
        <div className="col-md-8 blog-main">
            <div className="blog-post">
                <h2 id="blog-home" className="blog-post-title">{ currentPost.title }</h2>
                <p className="blog-post-meta">{ currentPost.created_at }</p>
                <hr />
                <p>{ currentPost.content }</p>
                <img src={currentPost.image} className="img-fluid w-100" alt="for react roiter" />
            </div>
            <nav className="blog-pagination">
                <a href="#blog-home" className="mr-2"><button className="btn btn-outline-primary" onClick={this.prePost }>Older</button></a>
                <a href="#blog-home"><button className="btn btn-outline-secondary" onClick={this.nextPost}>Newer</button></a>
            </nav>
        </div>
    );
  }
}

export default BlogHome;