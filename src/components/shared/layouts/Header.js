import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import Form from './Form';

class Header extends Component {
    render() {
        return (
            <header className="blog-header py-3">
                <div className="row flex-nowrap justify-content-between align-items-center">
                    <div className="col-4 pt-1">
                        <a className="text-muted" href="abc.vn">Subscribe</a>
                    </div>
                    <div className="col-4 text-center">
                        <Link className="blog-header-logo text-dark" to={'/'}>Large</Link>
                    </div>
                    <div className="col-4 d-flex justify-content-end align-items-center">
                        <a className="text-muted" href="abc.vn">
                            <svg xmlns="http://www.w3.org/2000/svg" width={20} height={20} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="mx-3"><circle cx="10.5" cy="10.5" r="7.5" /><line x1={21} y1={21} x2="15.8" y2="15.8" /></svg>
                        </a>
                        <a className="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#signup">Sign up</a>
                        <div className="modal fade" id="signup" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <Form />
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;