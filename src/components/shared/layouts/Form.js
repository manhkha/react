import React, { Component } from 'react';
import axios from 'axios';
import swal from 'sweetalert';
import { store } from '../../../store/index';


class Form extends Component {
    
    handleChange = (e) => {
        store.dispatch({type: 'SIGN_UP', [e.target.id]: e.target.value })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let { name, email, password, password_confirmation} = store.getState().infoUser;
        if (name === '' || email === '' || password === '' || password_confirmation ===  '' ){
            store.dispatch({ type: 'ERROR', errorMessage: 'Can not empty field'})
            return;
        }
        if (password !== password_confirmation){
            alert('Password confirmation not match with password');
            return;
        }

        const user = store.getState().infoUser;
        this.handleClickSubmit();
        axios.post('https://blogapireact.herokuapp.com/signup', {user})
             .then(res => {
                 if (res.data.status === 'ok'){
                    swal({
                        title: "Good job!",
                        text: res.data.message,
                        icon: "success",
                        button: "OK",
                    });
                 }
             })
             .catch(err => {
                 console.log(err);
             })
    }

    handleClickSubmit = () => {
        const closeForm = document.getElementById('close-form');
        closeForm.click();
    }

    render() {
        let { errors } = store.getState();
        let checkError = errors !== '' ? true : false
        let divError;
        if (checkError){
            divError = (<div className='text-center bg-danger'><h3>{errors}</h3></div>);
        }
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Sign up</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" id='close-form'>&times;</span>
                    </button>
                </div>
                { divError }
                <div className="modal-body">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Name</label>
                            <input type="text" onChange={this.handleChange} className="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter name"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" onChange={this.handleChange} className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" onChange={this.handleChange} className="form-control" id="password" placeholder="Password" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" onChange={this.handleChange} className="form-control" id="password_confirmation" placeholder="Password confirmation" />
                        </div>
                        <div className="form-check">
                            <input onChange={this.handleChange} type="checkbox" className="form-check-input" id="exampleCheck1" />
                            <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                        </div><br />
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Form;