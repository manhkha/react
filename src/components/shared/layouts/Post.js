import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { store } from '../../../store/index';

class Post extends Component {
    componentDidMount(){
        axios.get('https://blogapireact.herokuapp.com/allpost')
             .then(res => {
                 store.dispatch(this.getTypePost(res.data.posts));
             })
             .catch(error => {
                 console.log(error);
             })
    }

    getTypePost = (text) => {
        return {
            type: 'ALL_POST',
            payload: text
        }
    }

    render() {
        var { posts } = store.getState();
        var res = '';
        if (posts.length > 0){
            res = posts.map((post, index) => (
                <div className="col-md-6" key={index}>
                    <div className="card flex-md-row mb-4 shadow-sm h-md-250">
                        <div className="card-body d-flex flex-column align-items-start">
                            <strong className="d-inline-block mb-2 text-primary">World</strong>
                            <h3 className="mb-0">
                                <Link className="text-dark" to={'/posts/' + post.id}>{post.title.substring(0,10)}</Link>
                            </h3>
                            <div className="mb-1 text-muted">{ post.created_at}</div>
                            <p className="card-text mb-auto">{post.content.substring(0, 50)}</p>
                        </div>
                        <img className="card-img-right flex-auto d-none d-lg-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" style={{ width: 200, height: 250 }} src={post.image} data-holder-rendered="true" />
                    </div>
                </div>
            ))
        }
        return (
            <div className="row mb-2">
                {res}
            </div>
        );
    }
}

export default Post;