import React, { Component } from 'react';

class CreatedPost extends Component {
  constructor(props){
    super(props);
    this.state  = {
      posts: []
    }
  }
  render() {
    return (
      <aside className="col-md-4 blog-sidebar">
        <div className="p-3">
            <h4 className="font-italic">Archives</h4>
            <ol className="list-unstyled mb-0">
                <li><a href="abc.vn">March 2014</a></li>
            </ol>
        </div>
    </aside>
    );
  }
}

export default CreatedPost;