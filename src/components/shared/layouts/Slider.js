import React, { Component } from 'react';
import Slide from '../../../utils/slide'

class Slider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentSlideIndex: 0,
            slider: [
                new Slide(
                    "The Walking Dead",
                    "A show about fighting zombies",
                    "https://acollectivemind.files.wordpress.com/2013/12/season-4-complete-cast-poster-the-walking-dead-the-walking-dead-35777405-2528-670.png",
                    "http://www.amc.com/shows/the-walking-dead",
                    0
                ),
                new Slide(
                    "The Big Bang Theory",
                    "A show about Sheldon",
                    "http://cdn.playbuzz.com/cdn/7b4f711d-2a46-45dc-bea7-85a15ffa587a/b9f955c1-d8ad-4778-862d-e66ec2c38df1.jpg",
                    "http://www.cbs.com/shows/big_bang_theory/",
                    1
                ),
                new Slide(
                    "The Last Man on Earth",
                    "A show about loneliness",
                    "https://www.wired.com/wp-content/uploads/2015/02/LMOE-AliveInTuscon_scene44_0028_hires2.jpg",
                    "http://www.fox.com/the-last-man-on-earth",
                    2
                )
            ]
        }
    }
    
    componentDidMount(){
        const currentSlideIndex = document.getElementById('slide' + this.state.currentSlideIndex)
        if(currentSlideIndex){
            currentSlideIndex.style.left = 0;
        }
    }

    nextSlide = () => {
        var nextSlideIndex;
        if (this.state.currentSlideIndex === (this.state.slider.length - 1)){
          nextSlideIndex = 0;
        }
        else {
          nextSlideIndex = this.state.currentSlideIndex + 1;
        }
        document.getElementById('slide' + nextSlideIndex).style.left = '100%';
        document.getElementById('slide' + this.state.currentSlideIndex).style.left = '0';
  
        document.getElementById('slide' + nextSlideIndex).setAttribute("class", "slideItem slideInRight");
        document.getElementById('slide' + this.state.currentSlideIndex).setAttribute("class", "slideItem slideOutLeft")
        this.setState({
            currentSlideIndex : nextSlideIndex
        });
      }
      prewSlide = () => {
        var prevSlideIndex;
        if (this.state.currentSlideIndex === 0){
          prevSlideIndex = this.state.slider.length -1 ;
        }
        else {
          prevSlideIndex = this.state.currentSlideIndex - 1;
        }
        document.getElementById('slide' + prevSlideIndex).style.left = '-100%';
        document.getElementById('slide' + this.state.currentSlideIndex).style.left = '0';
  
        document.getElementById('slide' + prevSlideIndex).setAttribute("class", "slideItem slideInLeft");
        document.getElementById('slide' + this.state.currentSlideIndex).setAttribute("class", "slideItem slideOutRight")
        this.setState({
            currentSlideIndex : prevSlideIndex
        });
      }

    render() {
        const slideHTML = this.state.slider.map(function (item) {
            return (<div key={item.id} id={item.id} className="slideItem" style={{backgroundImage: `url(${item.img_link})`}}>
            <div className="slideOverlay">
            <h1>{item.title}</h1>
            <h2>{item.caption} </h2>
            <a href={item.link}>click here</a>
            </div>
            </div>)
        });
        return (
            <div>
                <div id="slider">{slideHTML}</div>
                <div id="slideNav">
                    <div id="previousControl" className="b1" onClick={this.prewSlide}><i className="fa fa-arrow-circle-o-left"></i></div>
                    <div id="nextControl" className="b1" onClick={this.nextSlide}><i className="fa fa-arrow-circle-o-right"></i></div>
                </div>
            </div>
        );
    }
}

export default Slider;
