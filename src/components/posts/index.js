import React, { Component } from 'react';
import axios from 'axios';
import { store } from '../../store/index'; 
class Posts extends Component {
    
    componentDidMount() {
        this.getPostData(this.props.match.params.id);
    }
    getPostData = (posts_id) => {
        axios.get(`https://blogapireact.herokuapp.com/posts/${posts_id}`)
            .then(
                res => {
                    const post = res.data;
                    store.dispatch(this.getValue(post));
                }
            )
            .catch(function (error) {
                console.log(error);
            });
    }

    getValue = (data) => {
        return {
            type: 'DATA_POST',
            payload: data
        }
    }
    render() {
        let htmlData = '';
        const post = store.getState().post;
        htmlData = (
            <div className="jumbotron jumbotron-fluid" key={this.props.match.params.id}>
                <div className="container">
                    <h1  className="text-center post_title">{post.title}</h1>
                    <p className="post_time">{post.time}</p>
                    <div className="row">
                        <div className="col-6 post_content">
                        {post.content}
                        </div>
                        <div className="col-6">
                            <img src={post.image} className="img-fluid w-100" alt="for react roiter" />
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div>
                {htmlData}
            </div>
        );
    }
}

export default Posts;