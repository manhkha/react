import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Header from './components/shared/layouts/Header';
import Footer from './components/shared/layouts/Footer';
import Nav from './components/shared/layouts/Nav';
import Router from './routes';

class App extends Component {
  render() {
    return (
      <div className="container">
        <Header />
        <Nav />
        <Router />
        <Footer />
      </div>
    );
  }
}

export default App;
